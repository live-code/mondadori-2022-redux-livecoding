import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { selectLanguage, selectTheme } from '../../core/store/config.selectors';
import { changeLanguage, changeTheme } from '../../core/store/config.store';

export const SettingsPage = () => {
  const dispatch = useDispatch();
  const theme = useSelector(selectTheme);
  const lang = useSelector(selectLanguage);

  return <div>
    Settings {theme} {lang}
    <hr/>
    <button onClick={() => dispatch(changeTheme('light'))} disabled={theme === 'light'}>light</button>
    <button onClick={() => dispatch(changeTheme('dark'))} disabled={theme === 'dark'}>dark</button>
    <button onClick={() => dispatch(changeLanguage('it'))} disabled={lang === 'it'}>it</button>
    <button onClick={() => dispatch(changeLanguage('en'))} disabled={lang === 'en'}>en</button>
  </div>
};
