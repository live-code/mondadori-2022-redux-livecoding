import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { User } from '../../../model/user';

export const usersApi = createApi({
  reducerPath: 'users',
  baseQuery: fetchBaseQuery({ baseUrl: 'http://localhost:3001/'}),
  // refetchOnMountOrArgChange: true,
  tagTypes: ['Users'],
  endpoints: builder => ({
    getUsers: builder.query<{ id: number, title: string }[], void>({
      query: () => 'users',
      keepUnusedDataFor: 2,
      transformResponse: (res: User[]) => {
        return res.map(u => ({ id: u.id, title: u.name}))
      },
      providesTags: ['Users']
    }),
    getUserById: builder.query<User, number>({
      query: (id) => `users/${id}`,
    }),
    search: builder.query<User[], string>({
      query: (text) => `users?q=${text}`,
    }),
    deleteUser: builder.mutation({
      query: (id) => ({
        url: `users/${id}`,
        method: 'DELETE'
      }),
      invalidatesTags: ['Users']
    })
  })
})

export const {
  useGetUsersQuery,
  useGetUserByIdQuery,
  useSearchQuery,
  usePrefetch,
  useDeleteUserMutation
} = usersApi
