import { useDeleteUserMutation, useGetUserByIdQuery, useGetUsersQuery, useSearchQuery } from './store/users.api';

export const UsersPage = () => {
  const {
    data, isFetching, isLoading, isError, isSuccess
  } = useGetUsersQuery();

  const [deleteHandler, { isLoading: isLoadingDelete}] = useDeleteUserMutation();

  return <div>
    UsersPage
    { isError && <div>error</div>}
    { isLoading && <div>pending</div>}
    {
      data?.map(u =>
        <li key={u.id}>
          {u.title}
          <button onClick={() => deleteHandler(u.id)}>delete</button>
        </li>
      )
    }
  </div>
};
