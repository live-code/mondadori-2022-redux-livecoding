import { createReducer } from '@reduxjs/toolkit';
import { decrement, increment, reset } from './counter.actions';

const initialState = 0;

export const counterReducer = createReducer(initialState, builder =>
  builder
    .addCase(increment, (state, action) => state + action.payload)
    .addCase(decrement, (state, action) => state - action.payload)
    .addCase(reset, () => initialState)
    // .addCase(changeTheme, () => initialState)
)

/*
export function counterReducer(state = initialState, action: any) {
  switch (action.type) {
    case 'increment':
      return state + action.payload;
    case 'decrement':
      return state - action.payload;
    case 'reset':
      return initialState
  }
  return state;
}
*/
