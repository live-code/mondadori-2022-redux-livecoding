import { createAction } from '@reduxjs/toolkit';

export const increment = createAction<number>('increment')
export const decrement = createAction<number>('decrement')
export const reset = createAction('reset')


/*
export function increment(payload: number) {
  return {
    type: 'increment',
    payload
  }
}
*/
