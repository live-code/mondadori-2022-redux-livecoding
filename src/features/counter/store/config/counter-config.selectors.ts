import { RootState } from '../../../../App';

export const selectItemsPerBox = (state: RootState) => state.counter.config.itemsPerBox;
export const selectMaterial = (state: RootState) => state.counter.config.material;
