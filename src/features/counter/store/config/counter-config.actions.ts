import { createAction } from '@reduxjs/toolkit';

export const changeMaterial = createAction<'wood' | 'plastic'>('changeMaterial')
export const changeItemsPerBox = createAction<number>('changeItemsPerBox')
