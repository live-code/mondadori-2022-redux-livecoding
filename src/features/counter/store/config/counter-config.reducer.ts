import { createReducer } from '@reduxjs/toolkit';
import { reset } from '../counter/counter.actions';
import { changeItemsPerBox, changeMaterial } from './counter-config.actions';

export interface CounterConfigState {
  itemsPerBox: number;
  material: 'wood' | 'plastic'
}

const initialState: CounterConfigState = {
  itemsPerBox: 5,
  material: 'wood'
};

export const counterConfigReducer = createReducer(initialState, builder =>
    builder
      .addCase(changeMaterial, (state, action) => {
        if (action.payload === 'plastic') {
          state.itemsPerBox = 10;
        } else {
          state.itemsPerBox = 5;
        }
        state.material = action.payload
      })
      .addCase(changeItemsPerBox, (state, action) => {
        state.itemsPerBox = action.payload
      })
      .addCase(reset, () => initialState)
  // .addCase(changeTheme, () => initialState)
)
