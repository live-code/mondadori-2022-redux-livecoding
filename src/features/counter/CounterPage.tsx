import { useDispatch, useSelector } from 'react-redux';
import { changeItemsPerBox, changeMaterial } from './store/config/counter-config.actions';
import { selectItemsPerBox, selectMaterial } from './store/config/counter-config.selectors';
import { decrement, increment, reset } from './store/counter/counter.actions';
import { selectCounter, selectTotalBox } from './store/counter/counter.selectors';


export const CounterPage = () => {
  const dispatch = useDispatch()
  const counter = useSelector(selectCounter)
  const material = useSelector(selectMaterial)
  const itemsPerBox = useSelector(selectItemsPerBox)
  const totalBox = useSelector(selectTotalBox)

  return <div>
    <div>Total Items: {counter}</div>
    <div>Box: { totalBox } (Max {itemsPerBox} per box / {material})</div>
    <button>Confirm</button>
    <hr/>

    <button onClick={() => dispatch(changeMaterial('wood'))}>wood (5)</button>
    <button onClick={() => dispatch(changeMaterial('plastic'))}>Plastic (10)</button>
    <button onClick={() => dispatch(changeItemsPerBox(5))}>Box 5</button>
    <button onClick={() => dispatch(changeItemsPerBox(10))}>Box 10</button>
    <hr/>
    <button onClick={() => dispatch(decrement(5))}>-</button>
    <button onClick={() => dispatch(increment(10))}>+</button>
    <button onClick={() => dispatch(reset())}>reset</button>
  </div>
};
