import React from 'react';
import { useSelector } from 'react-redux';
import { useAppDispatch } from '../../../App';
import { Product } from '../../../model/product';
import { selectProductList, selectTotalCost } from '../store/products/product.selectors';
import { deleteProduct, toggleProduct } from '../store/products/products.actions';

interface ProductsListProps {
  products: Product[];
  total: number;
  onToggleVisibility: (p: Product) => void;
  onDeleteProduct: (id: number) => void;
}

export const ProductsList = (props: ProductsListProps) => {
  return  <div>
    {
      props.products.map(p => {
        return <li key={p.id}>
          {p.title} - € {p.price}
          <button onClick={() => props.onToggleVisibility(p)}>
            {p.visibility ? 'Visible' : 'Not Visible'}
          </button>
          <button onClick={() => props.onDeleteProduct(p.id)}>Delete</button>
        </li>
      })
    }
    <hr/>
    TOTAL COST: {props.total}
  </div>
};

