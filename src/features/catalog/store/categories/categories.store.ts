// features/catalog/store/categories.store.ts
import { createSlice } from '@reduxjs/toolkit';

export const categoriesStore = createSlice({
  name: '[products categories]',
  initialState: [
    { id: -1, name: 'All'},
    { id: 1, name: 'Latticini'},
    { id: 2, name: 'Frutta'},
  ],
  reducers: {}
})
