import { RootState } from '../../../../App';

export const selectCategories = (state: RootState) => state.catalog.categories
