import { combineReducers } from '@reduxjs/toolkit';
import { catalogFilterStore } from './filters/catalog-filters.store';
import { categoriesStore } from './categories/categories.store';
import { productsStore } from './products/products.store';

export const catalogReducers = combineReducers({
  products: productsStore.reducer,
  categories: categoriesStore.reducer,
  filters: catalogFilterStore.reducer
})
