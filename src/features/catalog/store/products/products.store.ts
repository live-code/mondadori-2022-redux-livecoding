import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Product } from '../../../../model/product';
import { getProducts } from './products.actions';

export const productsStore = createSlice({
  initialState: {
    list: [] as Product[],
    error: false,
    pending: false
  },
  name: 'products',
  reducers: {
    getProductsSuccess(state, action: PayloadAction<Product[]>) {
      // state.error = false;
      // state.list = action.payload
    },
    addProductSuccess(state, action: PayloadAction<Product>) {
      state.error = false;
      state.list.push(action.payload)
    },
    deleteProductSuccess(state, action: PayloadAction<number>) {
      state.error = false;
      const index = state.list.findIndex(p => p.id === action.payload)
      state.list.splice(index, 1);
    },
    toggleVisibilityProductSuccess(state, action: PayloadAction<Product>) {
      state.error = false;
      const productToUpdate = state.list.find(p => p.id === action.payload.id)
      if (productToUpdate) {
        productToUpdate.visibility = action.payload.visibility
      }
    },
    setError(state) {
      state.error = true;
    },
  },
  extraReducers: builder => builder
    .addCase(getProducts.fulfilled,(state, action: PayloadAction<Product[]>) => {
      state.error = false;
      state.pending = false;
      state.list = action.payload
    })
    .addCase(getProducts.rejected,(state) => {
      state.pending = false;
      state.error = true;
    })
    .addCase(getProducts.pending,(state) => {
      state.pending = true;
      state.error = true;
    })
})
export const {
  getProductsSuccess,
  setError,
  addProductSuccess,
  deleteProductSuccess,
  toggleVisibilityProductSuccess
} = productsStore.actions
