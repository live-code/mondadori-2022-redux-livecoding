import { createAction, createAsyncThunk, isRejectedWithValue } from '@reduxjs/toolkit';
import axios from 'axios';
import { AppThunk } from '../../../../App';
import { setHttpStatus } from '../../../../core/store/http-status.store';
import { Product } from '../../../../model/product';
import {
  addProductSuccess,
  deleteProductSuccess, setError,
  getProductsSuccess,
  toggleVisibilityProductSuccess
} from './products.store';

// createAsyncThunk
export const getProducts = createAsyncThunk<Product[], void>(
  'products/get',
  async (payload, { dispatch, rejectWithValue } ) => {
    try {
      const res = await axios.get<Product[]>('http://localhost:3001/products')
      return res.data;
    } catch (err) {
      return rejectWithValue('erroraccio!!!')
    }
  }
)
/**
 *  CLASSIC THUNK
 */
export const getProductsClassic = (): AppThunk => async (dispatch) => {
  try {
    const res = await axios.get<Product[]>('http://localhost:3001/products')
    dispatch(getProductsSuccess(res.data))
    dispatch(setHttpStatus({ status: 'success', actionType: 'get'}))
  } catch (err) {
    dispatch(setError())
    dispatch(setHttpStatus({ status: 'error', actionType: 'get'}))
  }
}

export const deleteProduct = (id: number): AppThunk => async dispatch => {
  try {
    await axios.delete<void>(`http://localhost:3001/products/${id}`);
    dispatch(deleteProductSuccess(id))
  } catch (err) {
    dispatch(setError())

  }
};

export const addProduct = (
  product: Partial<Product>
): AppThunk => async dispatch => {
  try {
    const res = await axios.post<Product>(`http://localhost:3001/products/`, {
      ...product,
      visibility: false
    })
    dispatch(addProductSuccess(res.data))
  } catch (err) {
    dispatch(setError())

  }
}


export const toggleProduct = (product: Product): AppThunk => async dispatch => {
  try {
    const res = await axios.patch<Product>(`http://localhost:3001/products/` + product.id, {
      ...product,
      visibility: !product.visibility
    })
    dispatch(toggleVisibilityProductSuccess(res.data))
  } catch (err) {
    dispatch(setError())
  }
}
