import { RootState } from '../../../../App';

export const selectProductList = (state: RootState) => state.catalog.products.list;
export const selectProductListPending = (state: RootState) => state.catalog.products.pending;
export const selectProductListError = (state: RootState) => state.catalog.products.error;
export const selectProductListByCat = (state: RootState) => state.catalog.products.list.filter(p => {
  if (state.catalog.filters.catId === -1) {
    return state.catalog.products.list
  }
  return p.categoryID === state.catalog.filters.catId
});
export const selectTotalProducts = (state: RootState) => state.catalog.products.list.length;
export const selectTotalCost = (state: RootState) =>
  state.catalog.products.list.reduce((acc, product) => acc + product.price, 0)

/*
export const selectTotalCost = (state: RootState) => {
  let total = 0;
  state.catalog.forEach(p => {
    total += p.price
  })
  return total;
}

*/
