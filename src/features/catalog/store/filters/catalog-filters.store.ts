import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../../../App';

export const catalogFilterStore = createSlice({
  name: '[catalog filter]',
  initialState: {
    catId: -1
  },
  reducers: {
    changeCategoryId(state, action: PayloadAction<number>) {
      state.catId = action.payload;
    }
  }
})
export const { changeCategoryId } = catalogFilterStore.actions
// selectors
export const selectCatalogFilters = (state: RootState) => state.catalog.filters;
