import classNames from 'classnames';
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { useAppDispatch } from '../../App';
import { gethttpStatus } from '../../core/store/http-status.store';
import Card from '../../shared/Card';
import { ProductsForm, ProductsList } from './components';
import { selectCategories } from './store/categories/categories.selectors';
import { changeCategoryId, selectCatalogFilters } from './store/filters/catalog-filters.store';
import {
  selectProductList,
  selectProductListByCat,
  selectProductListError, selectProductListPending,
  selectTotalCost
} from './store/products/product.selectors';
import { addProduct, deleteProduct, getProducts, toggleProduct } from './store/products/products.actions';

export const CatalogPage = () => {
  const dispatch = useAppDispatch();
  const products = useSelector(selectProductListByCat)
  const categories = useSelector(selectCategories)
  const totalCost = useSelector(selectTotalCost)
  const filters = useSelector(selectCatalogFilters)
  const pending = useSelector(selectProductListPending)

  useEffect(() => {
    dispatch(getProducts())
      .then(res => {
        console.log(res.payload)
      })
  },[] )
  


  return <div>

    <StatusMessages />
    { pending && <div>loading...</div> }

    <ProductsForm
      categories={categories}
      onSubmit={data => dispatch(addProduct(data))}
    />

    {/*FILTERS*/}
    {
      categories.map(c => {
        return <button
          key={c.id}
          className={classNames({'bg-dark': c.id === filters.catId})}
          onClick={() => dispatch(changeCategoryId(c.id))}
        >{c.name}</button>
      })
    }

    <hr/>
    <ProductsList
      products={products}
      total={totalCost}
      onToggleVisibility={(p) => dispatch(toggleProduct(p))}
      onDeleteProduct={id => dispatch(deleteProduct(id))}
    />
  </div>
};


const StatusMessages = () => {
  const httpStatus = useSelector(gethttpStatus)

  return <div>
    {httpStatus.status === 'error' && httpStatus.actionType === 'get' &&
      <div className="alert alert-danger">Product not loaded!</div>}

    {httpStatus.status === 'success' && httpStatus.actionType === 'get' &&
      <div className="alert alert-success">Product  loaded!</div>}
  </div>
}
