import { RootState } from '../../../App';

export const selectNews = (state: RootState) => {
  switch (state.home.filters.visibility) {
    case 'published': return state.home.news.filter(n => n.published);
    case 'unpublished': return state.home.news.filter(n => !n.published);
    default: return state.home.news;
  }
}
