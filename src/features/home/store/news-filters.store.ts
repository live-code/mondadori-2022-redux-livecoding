import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export const newsFiltersStore = createSlice({
  name: 'newsFilters',
  initialState: {
    visibility: 'all'
  },
  reducers: {
    setVisibility(state, action: PayloadAction<'all' | 'published' | 'unpublished'>) {
      state.visibility = action.payload
    }
  }
})

export const { setVisibility } = newsFiltersStore.actions
