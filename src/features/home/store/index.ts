import { combineReducers } from '@reduxjs/toolkit';
import { articlesStore } from './articles.store';
import { newsFiltersStore } from './news-filters.store';
import { newsStore } from './news.store';

export const homeReducers = combineReducers({
  news: newsStore.reducer,
  articles: articlesStore.reducer,
  filters: newsFiltersStore.reducer,
  hero: () => ({ title: 'bla bla', desc: 'lorem...'})
})
