import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { News } from '../../../model/news';

const initialState: News[] = [];

export const newsStore = createSlice({
  name: 'news',
  initialState,
  reducers: {
    addNews(state, action: PayloadAction<string>) {
      state.push({
        id: Date.now(),
        title: action.payload,
        published: false,
      })
    },
    deleteNews(state, action: PayloadAction<number>) {
      const index = state.findIndex(news => news.id === action.payload)
      state.splice(index, 1)
    },
    toggleNews(state, action: PayloadAction<number>) {
      const newsToUpdate = state.find(news => news.id === action.payload)
      if (newsToUpdate) {
        newsToUpdate.published = !newsToUpdate.published;
      }
    }
  }
})

export const { addNews, deleteNews, toggleNews } = newsStore.actions;
