import { useDispatch, useSelector } from 'react-redux';
import { setVisibility } from './store/news-filters.store';
import { selectNews } from './store/news.selectors';
import { addNews, deleteNews, toggleNews } from './store/news.store';

export const HomePage = () => {
  const dispatch = useDispatch();
  const newsList = useSelector(selectNews);

  function addNewsHandler(e: React.KeyboardEvent<HTMLInputElement>) {
    if (e.key === 'Enter') {
      dispatch(addNews(e.currentTarget.value))
      e.currentTarget.value = '';
    }
  }

  function setFilterHandler(e: React.ChangeEvent<HTMLSelectElement>) {
    dispatch(setVisibility(e.currentTarget.value as 'all' | 'published' | 'unpublished'))
  }

  return <div>
    <h1>News</h1>

    <input type="text" onKeyDown={addNewsHandler}/>
    <select className="form-control" onChange={setFilterHandler}>
      <option value="all">Show all</option>
      <option value="published">Published only</option>
      <option value="unpublished">Unpublished only</option>
    </select>

    <hr/>
    <ul>
    {
      newsList.map(news => {
        return <li
          key={news.id}
          style={{ color: news.published ? 'black' : 'gray'}}
        >
          {news.title} - {news.id}
          <i
            className="fa fa-trash"
            onClick={() => dispatch(deleteNews(news.id))}
          ></i>

          <i
            className="fa fa-eye"
            onClick={() => dispatch(toggleNews(news.id))}
          ></i>
        </li>
      })
    }
    </ul>
  </div>
};
