import { RootState } from '../../App';

export const selectTheme = (state: RootState) => state.config.theme
export const selectLanguage = (state: RootState) => state.config.language
