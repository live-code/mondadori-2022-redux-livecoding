import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { reset } from '../../features/counter/store/counter/counter.actions';
import { Config, Language, Theme } from '../../model/config';

const initialState: Config = { theme: 'dark', language: 'it'}

export const configStore = createSlice({
  name: 'config',
  initialState,
  reducers: {
    changeTheme(state, action: PayloadAction<Theme>) {
      state.theme = action.payload;
    },
    changeLanguage(state, action: PayloadAction<Language>) {
      state.language = action.payload
    }
  },
  extraReducers: builder => builder
    .addCase(reset, (state) => {
      state.theme = 'dark'
      state.language = 'it'
    })
})

export const { changeTheme, changeLanguage } = configStore.actions
