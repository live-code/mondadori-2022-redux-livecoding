import { AnyAction, combineReducers, configureStore, ThunkAction, ThunkDispatch } from '@reduxjs/toolkit';
import React from 'react';
import { Root } from 'react-dom/client';
import { Provider, useDispatch } from 'react-redux';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
import { Navbar } from './core/components/Navbar';
import { configStore } from './core/store/config.store';
import { httpStatusStore } from './core/store/http-status.store';
import { CatalogPage } from './features/catalog/CatalogPage';
import { catalogReducers } from './features/catalog/store';
import { categoriesStore } from './features/catalog/store/categories/categories.store';
import { productsStore } from './features/catalog/store/products/products.store';
import { CounterPage } from './features/counter/CounterPage';
import { counterReducers } from './features/counter/store';
import { counterConfigReducer } from './features/counter/store/config/counter-config.reducer';
import { counterReducer } from './features/counter/store/counter/counter.reducer';
import { HomePage } from './features/home/HomePage';
import { homeReducers } from './features/home/store';
import { newsStore } from './features/home/store/news.store';
import { SettingsPage } from './features/settings/SettingsPage';
import { usersApi } from './features/users/store/users.api';
import { UsersPage } from './features/users/UsersPage';
import './App.css';

const rootReducer = combineReducers({
  config: configStore.reducer,
  counter: counterReducers,
  home: homeReducers,
  catalog: catalogReducers,
  httpStatus: httpStatusStore.reducer,
  [usersApi.reducerPath]: usersApi.reducer
})
export const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat([usersApi.middleware])
})

export type RootState = ReturnType<typeof rootReducer>;
export type AppThunk = ThunkAction<void, RootState, null, AnyAction>
export type AppDispatch = ThunkDispatch<RootState, null, AnyAction>;
export const useAppDispatch = () => useDispatch<AppDispatch>()

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Navbar />
        <hr/>
        <Routes>
          <Route path="/home" element={ <HomePage /> } />
          <Route path="/counter" element={ <CounterPage /> } />
          <Route path="/users" element={ <UsersPage /> } />
          <Route path="/settings" element={ <SettingsPage /> } />
          <Route path="/catalog" element={ <CatalogPage /> } />
          <Route path="*" element={
            <Navigate to="/home" />
          } />
        </Routes>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
