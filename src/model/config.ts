export type Theme = 'dark' | 'light';
export type Language = 'it' | 'en';

export interface Config {
  theme: Theme;
  language: Language;
}
